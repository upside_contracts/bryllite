## Deployment Flow
- Deploy 'BrylliteToken.sol'. The listing can be done by a standard private key or multi-sig key, which is the owner of the contract.
  The Brillite Token, inheriting from custom PausableToken, gets deployed by this contract.
  An admin account address and the total amount of tokens are passed and used to initialize the Bryllite token.
  Token transfers are initially only alowed for the owner of the contract and admin account.
  
  
## Deployment Config
- In '/migrations/2_deploy_contracts.js', we may specify the admin account address and total number of tokens. 
  E.g.
  
  '''
  var admin = '0x123';
  var totalTokenAmount = 1000;
  '''