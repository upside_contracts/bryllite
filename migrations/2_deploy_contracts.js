var Token = artifacts.require("./BrylliteToken.sol");

var tokenContract;

module.exports = function(deployer) {
    var admin = "0x9A375DA5DEaF7f2769C6d1B13771CeFDe69cd0b1"; 
    var totalTokenAmount = 1 * 1000000000 * 1000000000000000000;

    return Token.new(admin, totalTokenAmount).then(function(result) {
        tokenContract = result;
    });
};
